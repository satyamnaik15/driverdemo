package door.cyron.house.housedoor.signin

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import door.cyron.house.housedoor.R
import door.cyron.house.housedoor.acount.OtpVerificationActivity
import door.cyron.house.housedoor.callbacks.Request
import door.cyron.house.housedoor.callbacks.ResponseListener
import door.cyron.house.housedoor.retrofit.RetrofitClient
import door.cyron.house.housedoor.retrofit.RetrofitRequest
import okhttp3.Headers

class SininActivity : AppCompatActivity() {

    lateinit var btnSignin: Button
    lateinit var editText: EditText
    lateinit var progressBar: ProgressBar
    private var request: Request? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sinin)
        btnSignin = findViewById<Button>(R.id.otp)
        editText = findViewById<EditText>(R.id.editText)
        progressBar = findViewById<ProgressBar>(R.id.progressBar)
        progressBar.visibility = View.GONE
//        editText.setText("918073037645")
        btnSignin.setOnClickListener(View.OnClickListener {
            //             val intent = (Intent(this@SininActivity, OtpVerificationActivity::class.java))
//             startActivity(intent)
//             finish()

            callApi(editText.text.toString())
        })
    }

    private fun callApi(phNo: String) {
        progressBar.visibility = View.VISIBLE
        Log.d("SIGNINACTIVITY", "callApi")
        val call = RetrofitClient.getAPIInterface()
            .sendOtp("http://indiaapi.ourschoolzone.org/api/sendOTPDriver?mobile_number=" + phNo)
        request = RetrofitRequest(call, object : ResponseListener<Signin> {
            override fun onResponse(response: Signin, headers: Headers) {

                progressBar.visibility = View.GONE
                Log.d("SIGNINACTIVITY", "onResponse")
                if (response.response == null) {
                    Toast.makeText(baseContext, "NOT REGISTER", Toast.LENGTH_SHORT).show()
                } else if (response.response?.responseval == 1) {
                    Toast.makeText(baseContext, "OTP SENT TOMOBILE NUMBER", Toast.LENGTH_SHORT).show()
                    val intent = (Intent(this@SininActivity, OtpVerificationActivity::class.java))
                    intent.putExtra("phNo", editText.text.toString())
                    startActivity(intent)

                }
            }

            override fun onError(status: Int, errors: String) {
                progressBar.visibility = View.GONE
                Log.d("SIGNINACTIVITY", "onError")
                Toast.makeText(baseContext, "ERROR", Toast.LENGTH_SHORT).show()

            }

            override fun onFailure(throwable: Throwable) {
                progressBar.visibility = View.GONE
                Log.d("SIGNINACTIVITY", "onFailure")
                Toast.makeText(baseContext, "FAILURE", Toast.LENGTH_SHORT).show()

            }
        })
        (request as RetrofitRequest<Signin>).enqueue()
    }
}
