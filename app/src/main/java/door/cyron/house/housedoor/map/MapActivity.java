package door.cyron.house.housedoor.map;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import door.cyron.house.housedoor.R;

public class MapActivity extends LocationBaseClass implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, View.OnClickListener {

    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private Marker myMarker;
    private ProgressBar progress;
    private Button btnStart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        btnStart = (Button) findViewById(R.id.btnStart);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        getLocation();

        progress = findViewById(R.id.progress);
        progress.setVisibility(View.VISIBLE);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.setVisibility(View.VISIBLE);
                getLocation();

            }
        });

    }

    @Override
    public void onLocationReceived(String lat, String log) {
        Utility.showSnackBar(mapFragment.getView(), "Location Receved");
        LatLng ownLoc = new LatLng(Double.parseDouble(lat), Double.parseDouble(log));
        mMap.clear();


        mMap.setOnMarkerClickListener(this);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(ownLoc));
        myMarker = mMap.addMarker(new MarkerOptions().position(ownLoc).
                icon(BitmapDescriptorFactory.fromBitmap(
                        Utility.createCustomMarker(MapActivity.this))));
        myMarker.setTag("OWN_LOCATION");

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ownLoc, 15f));

        progress.setVisibility(View.GONE);
    }

    @Override
    public void onError(String error) {
        Utility.showSnackBar(mapFragment.getView(), "Error occur");

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try {
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            googleMap.getUiSettings().setZoomControlsEnabled(true);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        if (!marker.getTag().equals("OWN_LOCATION")) {
            MapMarker mapMarker = (MapMarker) marker.getTag();
            LatLng latLng = new LatLng(mapMarker.getLatitude(), mapMarker.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f));
        }
        return false;
    }

    @Override
    public void onClick(View v) {

        getLocation();
        progress.setVisibility(View.VISIBLE);
    }

}
