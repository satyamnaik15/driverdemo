package door.cyron.house.housedoor.retrofit;


import door.cyron.house.housedoor.acount.OtpModel;
import door.cyron.house.housedoor.map.StartLocModel;
import door.cyron.house.housedoor.signin.Signin;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface APIInterface {


//    @FormUrlEncoded
//    @POST("login")
//    Call<SignInResponseModel> signIn(@Field("email") String email, @Field("type") String type, @Field("pass") String pass);

    /* @POST("login")
     Call<SignInResponseModel> signIn(@Body SigninModel signinModel);

     @POST("home")
     Call<HomeModel> homeDetails(@Body SigninModel signinModel);
 */
    @GET("")
    Call<Signin> sendOtp(@Url String url);

    @POST("")
    Call<StartLocModel> startLoc(@Url String url);

    @GET("")
    Call<StartLocModel> startLocFirst(@Url String url);

    @GET("")
    Call<OtpModel> otpVerification(@Url String url);

    interface Header {
        String AUTHORIZATION = "Authorization";
        String TIMEZONE = "Timezone";

    }

//    @Multipart
//    @POST("patients/register")
//    Call<Response> register(@Part MultipartBody.Part file, @PartMap() Map<String, RequestBody> partMap);


//    @GET("configurations")
//    Call<Configuration> downloadConfiguration();

//    @FormUrlEncoded
//    @POST("patients/get-otp")
//    Call<Response> requestOTP(@Field("uid") String mobileNo);

//    @POST
//    Call<Response> saveMedicineTrackerInfo(@Url String url, @Body SaveMedicineReadingRequestModel medicine);

}
