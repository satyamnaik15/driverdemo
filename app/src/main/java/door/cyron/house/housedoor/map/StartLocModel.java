package door.cyron.house.housedoor.map;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class StartLocModel {

    @SerializedName("Response")
    private ResponseEntity response;
    @SerializedName("BusLocations")
    private List<BusLocations> buslocations;

    public ResponseEntity getResponse() {
        return response;
    }

    public void setResponse(ResponseEntity response) {
        this.response = response;
    }

    public List<BusLocations> getBuslocations() {
        return buslocations;
    }

    public void setBuslocations(List<BusLocations> buslocations) {
        this.buslocations = buslocations;
    }

    public static class ResponseEntity {
        @SerializedName("Reason")
        private String reason;
        @SerializedName("ResponseVal")
        private int responseval;

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public int getResponseval() {
            return responseval;
        }

        public void setResponseval(int responseval) {
            this.responseval = responseval;
        }
    }

    public static class BusLocations {
        @SerializedName("school_id")
        private String schoolId;
        @SerializedName("traveldate")
        private String traveldate;
        @SerializedName("stoplng")
        private String stoplng;
        @SerializedName("stoplat")
        private String stoplat;
        @SerializedName("startlong")
        private String startlong;
        @SerializedName("startlat")
        private String startlat;
        @SerializedName("busnumber")
        private String busnumber;
        @SerializedName("id")
        private int id;

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getTraveldate() {
            return traveldate;
        }

        public void setTraveldate(String traveldate) {
            this.traveldate = traveldate;
        }

        public String getStoplng() {
            return stoplng;
        }

        public void setStoplng(String stoplng) {
            this.stoplng = stoplng;
        }

        public String getStoplat() {
            return stoplat;
        }

        public void setStoplat(String stoplat) {
            this.stoplat = stoplat;
        }

        public String getStartlong() {
            return startlong;
        }

        public void setStartlong(String startlong) {
            this.startlong = startlong;
        }

        public String getStartlat() {
            return startlat;
        }

        public void setStartlat(String startlat) {
            this.startlat = startlat;
        }

        public String getBusnumber() {
            return busnumber;
        }

        public void setBusnumber(String busnumber) {
            this.busnumber = busnumber;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
