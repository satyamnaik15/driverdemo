package door.cyron.house.housedoor.signin

import com.google.gson.annotations.SerializedName

class Signin {

    @SerializedName("Response")
    var response: ResponseEntity? = null

    class ResponseEntity {
        @SerializedName("Reason")
        var reason: String? = null
        @SerializedName("ResponseVal")
        var responseval: Int = 0
    }
}
