package door.cyron.house.housedoor.splash

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import door.cyron.house.housedoor.R
import door.cyron.house.housedoor.signin.SininActivity
import java.util.*
import kotlin.concurrent.timerTask

class SPlashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)


        val timer = Timer()
        timer.schedule(timerTask {
            val intent = (Intent(this@SPlashActivity, SininActivity::class.java))
            startActivity(intent)
            finish()
        }, 5000)
    }
}
