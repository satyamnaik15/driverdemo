package door.cyron.house.housedoor.map;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.*;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import static door.cyron.house.housedoor.map.Constant.MAP.*;

/**
 * Created by Satyam Kumar Naik on 12/12/2017.
 */

public abstract class LocationBaseClass extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback mLocationCallback;
    private LocationSettingsRequest.Builder builder;
    private Task<LocationSettingsResponse> result;
    private boolean forOnce = true;


    public abstract void onLocationReceived(String lat, String log);

    public abstract void onError(String error);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stopLocationUpdates();//stop if already called
        setLocationRequest();
    }

    public void setLocationRequest() {
        if (fusedLocationClient == null)
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (locationRequest == null) {
            locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(2000);
            locationRequest.setFastestInterval(1000);
            builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            builder.setNeedBle(true);
            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************
            result = LocationServices.getSettingsClient(LocationBaseClass.this).checkLocationSettings(builder.build());
        }
    }

    public void getLocation() {
        forOnce = true;
        if (Utility.isLocationPermitted(LocationBaseClass.this)) {
            getLastLocation();
        } else {
            requestLocationPermission();
        }
    }

    public void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, ACCESS_FINE_LOCATION_PERMISSION)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
            Utility.showAlert(this, false, "Need permission", "enable", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Utility.openAppSettingScreen(LocationBaseClass.this);
                }
            });
        } else
            ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION_PERMISSION}, MY_PERMISSIONS_REQUEST_LOCATION);
    }


    private void getLastLocation() {
        stopLocationUpdates();
        setLocationRequest();
        buildGoogleApiClient();
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                onLocationReceived(Double.toString(locationResult.getLastLocation().getLatitude()), Double.toString(locationResult.getLastLocation().getLongitude()));
                if (forOnce) {
                    stopLocationUpdates();
                    forOnce = false;
                }

            }
        };
    }

    private synchronized void buildGoogleApiClient() {
        if (googleApiClient == null)
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        if (googleApiClient != null && !googleApiClient.isConnected())
            googleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(Task<LocationSettingsResponse> task) {
                try {

                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (ActivityCompat.checkSelfPermission(LocationBaseClass.this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        fusedLocationClient.requestLocationUpdates(locationRequest,
                                mLocationCallback, Looper.myLooper());
                    }

                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(
                                        LocationBaseClass.this,
                                        GPS_REQUEST_CODE);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                                onError(e.toString());
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                                onError(e.toString());
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            onError("LOCATION SETTING UNAVAILABLE");
                            break;
                    }
                }
            }
        });

    }

    @Override
    public void onConnectionSuspended(int i) {
        onError("ON CONNECTION SUSPENDED");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        onError("ON CONNECTION FAILED");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case GPS_REQUEST_CODE:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        //after clicking ok on resume will get called and there we are requesting for location
                        if (ActivityCompat.checkSelfPermission(LocationBaseClass.this,
                                android.Manifest.permission.ACCESS_FINE_LOCATION)
                                == PackageManager.PERMISSION_GRANTED) {

                            fusedLocationClient.requestLocationUpdates(locationRequest,
                                    mLocationCallback, Looper.myLooper());
                        }

                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(this, "Need GPS", Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    default:
                        break;

                }
                break;
            case MY_PERMISSIONS_REQUEST_LOCATION:
                Toast.makeText(this, "Got IT !!!!", Toast.LENGTH_SHORT).show();
                getLocation();
                break;
        }

    }

    public void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        if (fusedLocationClient != null && mLocationCallback != null)
            fusedLocationClient.removeLocationUpdates(mLocationCallback);
        if (googleApiClient != null && googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLastLocation();
                } else {
//                    if (shouldShowLocationPopUp) {
//                        Utility.showAlert(LocationBaseClass.this,false, "Need permission", "enable", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                Utility.openAppSettingScreen(LocationBaseClass.this);
//                            }
//                        });
//                        shouldShowLocationPopUp = false;
//                    } else {
                    Toast.makeText(this, "Need permission", Toast.LENGTH_SHORT).show();
                    finish();
//                    }
                }

                return;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
    }
}
