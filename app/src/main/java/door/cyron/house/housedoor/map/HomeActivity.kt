package door.cyron.house.housedoor.map

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import door.cyron.house.housedoor.R
import door.cyron.house.housedoor.callbacks.Request
import door.cyron.house.housedoor.callbacks.ResponseListener
import door.cyron.house.housedoor.retrofit.RetrofitClient
import door.cyron.house.housedoor.retrofit.RetrofitRequest
import door.cyron.house.housedoor.signin.Signin
import okhttp3.Headers
import java.lang.Double
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.concurrent.timerTask


class HomeActivity : LocationBaseClass(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener, View.OnClickListener {

    private var mMap: GoogleMap? = null
    private var mapFragment: SupportMapFragment? = null
    private var myMarker: Marker? = null
    lateinit var progress: ProgressBar
    lateinit var btnStart: Button
    lateinit var txtVal: TextView
    lateinit var latt: String
    lateinit var lon: String
    private var request: Request? = null
    private var timer = Timer()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        btnStart = findViewById<View>(R.id.btnStart) as Button
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
        getLocation()
        btnStart.visibility = View.INVISIBLE
        progress = findViewById(R.id.progress)
        txtVal = findViewById(R.id.txtVal)
        progress.visibility = View.VISIBLE
        btnStart.setBackgroundColor(Color.parseColor("#ff669900"))

        btnStart.setOnClickListener(View.OnClickListener {
            //            progress.setVisibility(View.VISIBLE)
//            getLocation()

            if (btnStart.text.equals("START")) {
                callFirstApi()

            } else {
                btnStart.setBackgroundColor(Color.parseColor("#ff669900"))
                timer?.cancel()
                callStopApi()
            }
        })

        txtVal.setText("Bus no:- " + intent.getStringExtra("busno") + "\n")
        txtVal.append("School Name:- " + intent.getStringExtra("schoolName") + "\n")
        txtVal.append("Driver Id:- " + intent.getStringExtra("driverId"))
        txtVal.append("School Id:- " + intent.getStringExtra("schoolId"))
    }

    override fun onPause() {
        super.onPause()
        if(!btnStart.text.equals("START")) {
            if (timer != null)
                timer.cancel()
            callStopApi()
        }
    }

    override fun onStop() {
        super.onStop()

    }

    private fun callFirstApi() {

        runOnUiThread(timerTask {
            btnStart.text = "STOP"
            btnStart.setBackgroundColor(Color.parseColor("#ffcc0000"))

//            progress.visibility = View.VISIBLE
        })

        Log.d("HOME_ACTIVITY", "AddBusLocation Api")
        val call = RetrofitClient.getAPIInterface()
            .startLocFirst("https://indiaapi.ourschoolzone.org/api/AddBusLocation?busnumber="+intent.getStringExtra("busno")
                    +"&trip=6&startlat="+latt+"&startlng="+lon+"&traveldate="+getDate()+"&schoolid="+intent.getStringExtra("schoolId"))
        request = RetrofitRequest(call, object : ResponseListener<StartLocModel> {
            override fun onResponse(response: StartLocModel, headers: Headers) {

                runOnUiThread(timerTask {
                    //                    progress.visibility = View.GONE
                    Log.d("HOME_ACTIVITY", "AddBusLocation onResponse")
                    if (response.response == null) {
//                    Toast.makeText(baseContext, "NOT REGISTER", Toast.LENGTH_SHORT).show()
                    } else if (response.response?.responseval == 1) {
//                        Toast.makeText(baseContext, "Sent", Toast.LENGTH_SHORT).show()
                        timer = Timer()
                        timer?.scheduleAtFixedRate(timerTask {
                            callStartApi()
                        }, 5000, 5000)

                    }
                })


            }

            override fun onError(status: Int, errors: String) {

                runOnUiThread(timerTask {
                    progress.visibility = View.GONE
                    Log.d("HOME_ACTIVITY", "onError")

                })
            }

            override fun onFailure(throwable: Throwable) {
                runOnUiThread(timerTask {
                    progress.visibility = View.GONE
                    Log.d("HOME_ACTIVITY", "onFailure")

                })
            }
        })
        (request as RetrofitRequest<Signin>).enqueue()
    }

    private fun getDate(): Any? {
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
        val formatted = current.format(formatter)
        return formatted
    }

    private fun callStartApi() {

        runOnUiThread(timerTask {
            btnStart.text = "STOP"
            btnStart.setBackgroundColor(Color.parseColor("#ffcc0000"))

//            progress.visibility = View.VISIBLE
        })

        Log.d("HOME_ACTIVITY", "START Api")
        val call = RetrofitClient.getAPIInterface()
            .startLoc("https://indiaapi.ourschoolzone.org/api/UpdateBusLocation?busnumber="+intent.getStringExtra("busno")
                    +"&trip=6&stoplat="+latt+"&stoplng="+lon+"&traveldate="+getDate()+"&schoolid="+intent.getStringExtra("schoolId"))
        request = RetrofitRequest(call, object : ResponseListener<StartLocModel> {
            override fun onResponse(response: StartLocModel, headers: Headers) {

                runOnUiThread(timerTask {
                    //                    progress.visibility = View.GONE
                    Log.d("HOME_ACTIVITY", "onResponse")
                    if (response.response == null) {
//                    Toast.makeText(baseContext, "NOT REGISTER", Toast.LENGTH_SHORT).show()
                    } else if (response.response?.responseval == 1) {
//                        Toast.makeText(baseContext, "Sent", Toast.LENGTH_SHORT).show()


                    }
                })


            }

            override fun onError(status: Int, errors: String) {

                runOnUiThread(timerTask {
                    progress.visibility = View.GONE
                    Log.d("HOME_ACTIVITY", "onError")

                })
            }

            override fun onFailure(throwable: Throwable) {
                runOnUiThread(timerTask {
                    progress.visibility = View.GONE
                    Log.d("HOME_ACTIVITY", "onFailure")
//                    Toast.makeText(baseContext, "FAILURE", Toast.LENGTH_SHORT).show()

                })
            }
        })
        (request as RetrofitRequest<Signin>).enqueue()
    }

    private fun callStopApi() {

        btnStart.text = "START"
        progress.visibility = View.VISIBLE
        Log.d("HOME_ACTIVITY", "STOP API")
        val call = RetrofitClient.getAPIInterface()
            .startLoc("https://indiaapi.ourschoolzone.org/api/UpdateStopLocation?busnumber="+intent.getStringExtra("busno")
                    +"&trip=6&stoplat="+latt+"&stoplng="+lon+"&traveldate="+getDate()+"&schoolid="+intent.getStringExtra("schoolId"))
        request = RetrofitRequest(call, object : ResponseListener<StartLocModel> {
            override fun onResponse(response: StartLocModel, headers: Headers) {

                progress.visibility = View.GONE
                Toast.makeText(baseContext, "Stopped", Toast.LENGTH_SHORT).show()
            }

            override fun onError(status: Int, errors: String) {
                progress.visibility = View.GONE
                Log.d("HOME_ACTIVITY", "onError")
                Toast.makeText(baseContext, "ERROR", Toast.LENGTH_SHORT).show()

            }

            override fun onFailure(throwable: Throwable) {
                progress.visibility = View.GONE
                Log.d("HOME_ACTIVITY", "onFailure")
                Toast.makeText(baseContext, "FAILURE", Toast.LENGTH_SHORT).show()

            }
        })
        (request as RetrofitRequest<Signin>).enqueue()
    }


    override fun onLocationReceived(lat: String, log: String) {
        Utility.showSnackBar(mapFragment?.getView(), "Location Receved")
        val ownLoc = LatLng(Double.parseDouble(lat), Double.parseDouble(log))
        mMap?.clear()
        latt = lat
        lon = log
        btnStart.visibility = View.VISIBLE
        mMap?.setOnMarkerClickListener(this)
        mMap?.moveCamera(CameraUpdateFactory.newLatLng(ownLoc))
        myMarker = mMap?.addMarker(
            MarkerOptions().position(ownLoc).icon(
                BitmapDescriptorFactory.fromBitmap(
                    Utility.createCustomMarker(this@HomeActivity)
                )
            )
        )
        myMarker?.setTag("OWN_LOCATION")

        mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(ownLoc, 15f))

        progress?.setVisibility(View.GONE)
    }

    override fun onError(error: String) {
        Utility.showSnackBar(mapFragment?.getView(), "Error occur")
        btnStart.visibility = View.VISIBLE
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        try {
            googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
            googleMap.uiSettings.isZoomControlsEnabled = true

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onMarkerClick(marker: Marker): Boolean {

        if (marker.tag != "OWN_LOCATION") {
            val mapMarker = marker.tag as MapMarker?
            val latLng = LatLng(mapMarker!!.latitude, mapMarker.longitude)
            mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
        }
        return false
    }

    override fun onClick(v: View) {

        getLocation()
        progress?.setVisibility(View.VISIBLE)
    }
}
