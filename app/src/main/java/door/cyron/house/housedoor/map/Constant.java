package door.cyron.house.housedoor.map;

import android.Manifest;

/**
 * Created by Satyam Kumar Naik on 3/27/2018.
 */

public abstract class Constant {


    public static final String FRAG_OFFER = "Offer_fragment";


    private static final String PACKAGE_NAME = "loc.cyron.cab";

    public interface MAP {
        int GPS_REQUEST_CODE = 1000;
        int MY_PERMISSIONS_REQUEST_LOCATION = 99;
        String ACTIVITY_EXTRA = PACKAGE_NAME + ".ACTIVITY_EXTRA";
        String LATEST_LATITUDE = PACKAGE_NAME + ".Latitude";
        String LATEST_LONGITUDE = PACKAGE_NAME + ".Longitude";
        String ACCESS_FINE_LOCATION_PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION;
    }


    public interface REQ_CODE {
        int SIGNUP_SUCESS = 12;
    }

    public interface BundleKeys {
        String MARKER = "Marker";
    }
}
