package door.cyron.house.housedoor.acount

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.Toast
import com.goodiebag.pinview.Pinview
import door.cyron.house.housedoor.R
import door.cyron.house.housedoor.callbacks.Request
import door.cyron.house.housedoor.callbacks.ResponseListener
import door.cyron.house.housedoor.map.HomeActivity
import door.cyron.house.housedoor.retrofit.RetrofitClient
import door.cyron.house.housedoor.retrofit.RetrofitRequest
import okhttp3.Headers


class OtpVerificationActivity : AppCompatActivity() {

    lateinit var pin: Pinview
    lateinit var btnClick: Button
    lateinit var progressBar: ProgressBar
    private var request: Request? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_verification)
        pin = findViewById(R.id.otp_view)
        btnClick = findViewById(R.id.btnClick)
        progressBar = findViewById<ProgressBar>(R.id.progressBar)
        progressBar.visibility = View.GONE

//        pin.value = "462591";
        var phNo = intent.getStringExtra("phNo")
        btnClick.setOnClickListener(View.OnClickListener {
            /*  val intent = (Intent(this@OtpVerificationActivity, MapActivity::class.java))
              startActivity( intent)
              finish()*/
            callApi(phNo, pin.value.toString())
        })

    }

    private fun callApi(phNo: String, otp: String) {
        progressBar.visibility = View.VISIBLE
        Log.d("SIGNINACTIVITY", "callApi")
        val call = RetrofitClient.getAPIInterface()
            .otpVerification("https://indiaapi.ourschoolzone.org/api/verifyOTPDriver?mobile_number=" + phNo + "&otp=" + otp)
        request = RetrofitRequest(call, object : ResponseListener<OtpModel> {
            override fun onResponse(response: OtpModel, headers: Headers) {

                progressBar.visibility = View.GONE
                Log.d("OTPACTIVITY", "onResponse")
                if (response.response?.responseval == 0) {
                    Toast.makeText(baseContext, "" + response.response?.reason, Toast.LENGTH_SHORT).show()
                } else if (response.response?.responseval == 1) {
                    Toast.makeText(baseContext, "" + response.response?.reason, Toast.LENGTH_SHORT).show()
                    val intent = (Intent(this@OtpVerificationActivity, HomeActivity::class.java))
                    if (response.schooldriver.size >= 1 && response.schooldriver[0].bus.size >= 1) {
                        intent.putExtra("busno", response.schooldriver[0].bus[0].busnumber)
                        intent.putExtra("schoolName", response.schooldriver[0].schoolName)
                        intent.putExtra("driverId", response.schooldriver[0].driverid)
                        intent.putExtra("schoolId", response.schooldriver[0].schoolId)
                    }else{
                        intent.putExtra("busno", "")
                        intent.putExtra("schoolName", "")
                        intent.putExtra("driverId", "")
                    }
                    startActivity(intent)
                    finish()

                }
            }

            override fun onError(status: Int, errors: String) {
                progressBar.visibility = View.GONE
                Log.d("OTPACTIVITY", "onError")
                Toast.makeText(baseContext, "ERROR", Toast.LENGTH_SHORT).show()

            }

            override fun onFailure(throwable: Throwable) {
                progressBar.visibility = View.GONE
                Log.d("OTPACTIVITY", "onFailure")
                Toast.makeText(baseContext, "FAILURE", Toast.LENGTH_SHORT).show()

            }
        })
        (request as RetrofitRequest<*>).enqueue()
    }
}
