package door.cyron.house.housedoor.acount;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OtpModel {

    @SerializedName("Response")
    private ResponseEntity response;
    @SerializedName("schooldriver")
    private List<SchooldriverEntity> schooldriver;

    public ResponseEntity getResponse() {
        return response;
    }

    public void setResponse(ResponseEntity response) {
        this.response = response;
    }

    public List<SchooldriverEntity> getSchooldriver() {
        return schooldriver;
    }

    public void setSchooldriver(List<SchooldriverEntity> schooldriver) {
        this.schooldriver = schooldriver;
    }

    public static class ResponseEntity {
        @SerializedName("Reason")
        private String reason;
        @SerializedName("ResponseVal")
        private int responseval;

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public int getResponseval() {
            return responseval;
        }

        public void setResponseval(int responseval) {
            this.responseval = responseval;
        }
    }

    public static class BusEntity {
        @SerializedName("busnumber")
        private String busnumber;

        public String getBusnumber() {
            return busnumber;
        }

        public void setBusnumber(String busnumber) {
            this.busnumber = busnumber;
        }
    }

    public static class SchooldriverEntity {
        @SerializedName("Bus")
        private List<BusEntity> bus;
        @SerializedName("Language")
        private String language;
        @SerializedName("SchoolLogo")
        private String schoollogo;
        @SerializedName("SchoolAddress")
        private String schooladdress;
        @SerializedName("DriverID")
        private String driverid;
        @SerializedName("ProfileImage")
        private String profileimage;
        @SerializedName("Gender")
        private String gender;
        @SerializedName("DOB")
        private String dob;
        @SerializedName("Email")
        private String email;
        @SerializedName("Mobile")
        private String mobile;
        @SerializedName("LastName")
        private String lastname;
        @SerializedName("MiddleName")
        private String middlename;
        @SerializedName("FirstName")
        private String firstname;
        @SerializedName("School_Name")
        private String schoolName;
        @SerializedName("School_id")
        private String schoolId;

        public List<BusEntity> getBus() {
            return bus;
        }

        public void setBus(List<BusEntity> bus) {
            this.bus = bus;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getSchoollogo() {
            return schoollogo;
        }

        public void setSchoollogo(String schoollogo) {
            this.schoollogo = schoollogo;
        }

        public String getSchooladdress() {
            return schooladdress;
        }

        public void setSchooladdress(String schooladdress) {
            this.schooladdress = schooladdress;
        }

        public String getDriverid() {
            return driverid;
        }

        public void setDriverid(String driverid) {
            this.driverid = driverid;
        }

        public String getProfileimage() {
            return profileimage;
        }

        public void setProfileimage(String profileimage) {
            this.profileimage = profileimage;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getMiddlename() {
            return middlename;
        }

        public void setMiddlename(String middlename) {
            this.middlename = middlename;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getSchoolName() {
            return schoolName;
        }

        public void setSchoolName(String schoolName) {
            this.schoolName = schoolName;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }
    }
}
